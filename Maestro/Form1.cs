﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;


namespace Maestro
{
    public partial class Form1 : Form
    {
        OptionsHandler Settings;
        DatabaseHandler DB;
        ViewListHandler TrackViewList;
        InfoTabHandler InfoTab;
        MusicPlayerHandler MPlayer;
        int totalTracks = 0;
        TrackItem[] TrackList;
        Random randomNum = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DB = new DatabaseHandler();
            DB.Connect();
            totalTracks = DB.LoadTracks(ref TrackList);
            Settings = new OptionsHandler();
            TrackViewList = new ViewListHandler();
            ReOrderTrackList(0);
            TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
            InfoTab = new InfoTabHandler(ref NameBox, ref ArtistBox, ref AlbumBox, ref DateBox, ref PlaysBox, ref FavIcon);
            MPlayer = new MusicPlayerHandler(ref MainPPButton, ref MainPrevTrackButton, ref MainNextTrackButton, totalTracks, TrackList, ref MusicTimer);
        }
        private void ClearInfoTab()
        {
            NameBox.Text = "";
            ArtistBox.Text = "";
            AlbumBox.Text = "";
            DateBox.Text = "";
            PlaysBox.Text = "";
            FavIcon.Visible = false;
            InfoTabPlayButton.Visible = false;
            InfoTabEditButton.Visible = false;
            InfoTabDeleteButton.Visible = false;
            AlbumMainCoverImg.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
         }
        private void ReOrderTrackList(int mode)
        {
            if(mode == 0 || mode == 2) // By number
            {
                TrackItem tmpMemoryTrack;
                for (int i = 0; i < totalTracks; i++)
                {
                    for (int j = 0; j < totalTracks - 1; j++)
                    {
                        if (Int32.Parse(TrackList[j].T_timestamp) < Int32.Parse(TrackList[j + 1].T_timestamp))
                        {
                            tmpMemoryTrack = new TrackItem(TrackList[j + 1].T_id, TrackList[j + 1].T_name, TrackList[j + 1].T_artist, TrackList[j + 1].T_album, TrackList[j + 1].T_addedDate, TrackList[j + 1].T_plays, TrackList[j + 1].T_favorite, TrackList[j + 1].T_photo, TrackList[j + 1].T_timestamp, TrackList[j + 1].T_audio);
                            TrackList[j + 1] = TrackList[j];
                            TrackList[j] = tmpMemoryTrack;
                        }
                    }
                }

                TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);

            }
            else if(mode == 1) // By plays
            {
                // Start reOrdering with bubble short algorithm
                TrackItem tmpMemoryTrack;
                for(int i=0; i<totalTracks; i++)
                {
                    for(int j=0; j<totalTracks-1; j++)
                    {
                        if(TrackList[j].T_plays < TrackList[j + 1].T_plays)
                        {
                            tmpMemoryTrack = new TrackItem(TrackList[j + 1].T_id, TrackList[j + 1].T_name, TrackList[j + 1].T_artist, TrackList[j + 1].T_album, TrackList[j + 1].T_addedDate, TrackList[j + 1].T_plays, TrackList[j + 1].T_favorite, TrackList[j + 1].T_photo, TrackList[j + 1].T_timestamp, TrackList[j + 1].T_audio);
                            TrackList[j + 1] = TrackList[j];
                            TrackList[j] = tmpMemoryTrack;
                        }
                    }
                }
                TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
            }
        }

        private int GetTrackPointer(string timestamp)
        {
            for (int i = 0; i <= totalTracks; i++)
            {
                if (TrackList[i].T_timestamp == timestamp)
                {
                    return i;
                }
            }
            return -1;
        }

        private void TrackViewList1_Click(object sender, EventArgs e)
        {
            if (TrackViewList1.SelectedItems.Count == 1)
            {
                int i = GetTrackPointer(TrackViewList1.SelectedItems[0].SubItems[5].Text);
                if (i >= 0)
                {
                    NameBox.Enabled = false;
                    ArtistBox.Enabled = false;
                    AlbumBox.Enabled = false;
                    UpdateCoverButton.Visible = false;
                    InfoTab.State = "preview";
                    InfoTabEditButton.BackgroundImage = Image.FromFile("Sources\\Images\\pencilGray.png");
                    InfoTab.setData(TrackList[i].T_name, TrackList[i].T_artist, TrackList[i].T_album, TrackList[i].T_addedDate, TrackList[i].T_plays, TrackList[i].T_favorite, TrackList[i].T_timestamp);
                    if (TrackList[i].T_photo != "none")
                    {
                        try
                        {
                            Image tmpImage;
                            using (var bmpTemp = new Bitmap("Sources\\Database\\TrackCover\\" + TrackList[i].T_photo))
                            {
                                tmpImage = new Bitmap(bmpTemp);
                                AlbumMainCoverImg.BackgroundImage = tmpImage;
                            }
                        }
                        catch (System.ArgumentException g)
                        {
                            AlbumMainCoverImg.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                        }
                    }
                    else
                    {
                        AlbumMainCoverImg.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                    }
                    InfoTabPlayButton.Visible = true;
                    InfoTabEditButton.Visible = true;
                    InfoTabDeleteButton.Visible = true;
                }
            }

        }

        private void InfoTabPlayButton_Click(object sender, EventArgs e)
        {
            Settings.playStyle = "Next";
            playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\right-arrow.png");
            MPlayer.SetAudioFile(TrackList[GetTrackPointer(InfoTab.Timestamp)].T_audio, TrackList[GetTrackPointer(InfoTab.Timestamp)].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[GetTrackPointer(InfoTab.Timestamp)].T_photo, TrackList[GetTrackPointer(InfoTab.Timestamp)].T_name);
            MPlayer.Play();
            UpdatePlayingTrackPlays();
        }

        private void UpdatePlayingTrackPlays()
        {
            // Update Plays Variable
            int trackPlays = TrackList[GetTrackPointer(MPlayer.TracksTimestamp)].T_plays + 1;
            string trackTimestamp = TrackList[GetTrackPointer(MPlayer.TracksTimestamp)].T_timestamp;
            string query = "UPDATE Tracks SET Plays='" + trackPlays + "' WHERE Timestamp='" + trackTimestamp + "'";
            DB.Update(query);
            totalTracks = DB.LoadTracks(ref TrackList);
            ReOrderTrackList(Settings.viewStyle);
            TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
            if (InfoTab.Timestamp == trackTimestamp)
            {
                InfoTab.updatePlays(trackPlays);
            }
        }
        private void MainPPButton_Click(object sender, EventArgs e)
        {
            if (MPlayer.Status == "Ready")
            {
                // Do nothing
            }
            else if (MPlayer.Status == "Playing")
            {
                MPlayer.Pause();
            }
            else
            {
                MPlayer.Play();
            }
        }

        private void MusicTimer_Tick(object sender, EventArgs e)
        {
            MPlayer.setDuration();
            progressBar1.Value = Convert.ToInt32(MPlayer.GetPositionDouble() / MPlayer.GetDurationDouble() * 100);
            TrackPositionLabel.Text = MPlayer.GetPositionString();
            TrackDurationLabel.Text = MPlayer.GetDuretionString();
            if (MPlayer.GetState() == "wmppsStopped")
            {
                MusicTimer.Enabled = false;
                TrackPositionLabel.Text = "00:00";
                TrackDurationLabel.Text = "00:00";
                MainPPButton.BackgroundImage = Image.FromFile("Sources\\Images\\play-button.png");
                if(Settings.playStyle == "Next")
                {
                    MainNextTrackButton.PerformClick();
                }
                else
                {
                    // Play randomly selected track
                    int randomTrack = randomNum.Next(0, totalTracks);
                    MPlayer.SetAudioFile(TrackList[randomTrack].T_audio, TrackList[randomTrack].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[randomTrack].T_photo, TrackList[randomTrack].T_name);
                    MPlayer.Play();
                    UpdatePlayingTrackPlays();
                }
            }
        }

        private void volumeBar_ValueChanged(object sender, EventArgs e)
        {
            MPlayer.SetVolume(volumeBar.Value);
            if(volumeBar.Value > 75)
            {
                pictureBox1.BackgroundImage = Image.FromFile("Sources\\Images\\volume-full.png");
            }
            else if(volumeBar.Value > 50)
            {
                pictureBox1.BackgroundImage = Image.FromFile("Sources\\Images\\volume-medium.png");
            }
            else if(volumeBar.Value > 0)
            {
                pictureBox1.BackgroundImage = Image.FromFile("Sources\\Images\\volume-low.png");
            }
            else if(volumeBar.Value == 0)
            {
                pictureBox1.BackgroundImage = Image.FromFile("Sources\\Images\\volume-mute.png");
            }
        }

        private void MainNextTrackButton_Click(object sender, EventArgs e)
        {
            if(MPlayer.TracksTimestamp != "")
            {
                int nextTrackPointer = GetTrackPointer(MPlayer.TracksTimestamp) + 1;
                Settings.playStyle = "Next";
                playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\right-arrow.png");
                if (nextTrackPointer >= totalTracks)
                {
                    MPlayer.SetAudioFile(TrackList[0].T_audio, TrackList[0].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[0].T_photo, TrackList[0].T_name);
                    MPlayer.Play();
                }
                else
                {
                    MPlayer.SetAudioFile(TrackList[nextTrackPointer].T_audio, TrackList[nextTrackPointer].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[nextTrackPointer].T_photo, TrackList[nextTrackPointer].T_name);
                    MPlayer.Play();
                }
                UpdatePlayingTrackPlays();
            }
        }

        private void MainPrevTrackButton_Click(object sender, EventArgs e)
        {
            if(MPlayer.TracksTimestamp != "")
            {
                int prevTrackPointer = GetTrackPointer(MPlayer.TracksTimestamp) - 1;
                Settings.playStyle = "Next";
                playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\right-arrow.png");
                if (prevTrackPointer < 0)
                {
                    MPlayer.SetAudioFile(TrackList[totalTracks - 1].T_audio, TrackList[totalTracks - 1].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[totalTracks - 1].T_photo, TrackList[totalTracks - 1].T_name);
                    MPlayer.Play();
                }
                else
                {
                    MPlayer.SetAudioFile(TrackList[prevTrackPointer].T_audio, TrackList[prevTrackPointer].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[prevTrackPointer].T_photo, TrackList[prevTrackPointer].T_name);
                    MPlayer.Play();
                }
                UpdatePlayingTrackPlays();
            }
        }

        private void TrackViewList1_DoubleClick(object sender, EventArgs e)
        {
            if (TrackViewList1.SelectedItems.Count == 1)
            {
                int i = GetTrackPointer(TrackViewList1.SelectedItems[0].SubItems[5].Text);
                if (i >= 0)
                {
                    InfoTab.setData(TrackList[i].T_name, TrackList[i].T_artist, TrackList[i].T_album, TrackList[i].T_addedDate, TrackList[i].T_plays, TrackList[i].T_favorite, TrackList[i].T_timestamp);
                    if (TrackList[i].T_photo != "none")
                    {
                        try
                        {
                            Image tmpImage;
                            using (var bmpTemp = new Bitmap("Sources\\Database\\TrackCover\\" + TrackList[i].T_photo))
                            {
                                tmpImage = new Bitmap(bmpTemp);
                                AlbumMainCoverImg.BackgroundImage = tmpImage;
                            }
                        }
                        catch (System.ArgumentException g)
                        {
                            AlbumMainCoverImg.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                        }

                    }
                    else
                    {
                        AlbumMainCoverImg.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                    }

                    InfoTabPlayButton.Visible = true;
                    InfoTabEditButton.Visible = true;
                    InfoTabDeleteButton.Visible = true;
                    Settings.playStyle = "Next";
                    playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\right-arrow.png");
                    MPlayer.SetAudioFile(TrackList[i].T_audio, TrackList[i].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[i].T_photo, TrackList[i].T_name);
                    MPlayer.Play();
                    UpdatePlayingTrackPlays();
                }
            }
        }

        private void InfoTabEditButton_Click(object sender, EventArgs e)
        {
            if (InfoTab.State == "preview")
            {
                InfoTab.State = "edit";
                NameBox.Enabled = true;
                ArtistBox.Enabled = true;
                AlbumBox.Enabled = true;
                UpdateCoverButton.Visible = true;
                InfoTabEditButton.BackgroundImage = Image.FromFile("Sources\\Images\\check-button.png");
            }
            else
            {
                InfoTab.State = "preview";
                NameBox.Enabled = false;
                ArtistBox.Enabled = false;
                AlbumBox.Enabled = false;
                UpdateCoverButton.Visible = false;
                string newName, newArtist, newAlbum;
                newName = NameBox.Text;
                newArtist = ArtistBox.Text;
                newAlbum = AlbumBox.Text;
                string query = "UPDATE Tracks SET Track_name=@newName,Artist=@newArtist,Track_album=@newAlbum WHERE Timestamp=@trackTimestamp";
                OleDbCommand uCommand = new OleDbCommand(query, DB.DBconn);
                uCommand.Parameters.AddWithValue("@newName", newName);
                uCommand.Parameters.AddWithValue("@newArtist", newArtist);
                uCommand.Parameters.AddWithValue("@newAlbum", newAlbum);
                uCommand.Parameters.AddWithValue("@trackTimestamp", InfoTab.Timestamp);
                DB.Update(query, uCommand);
                int totalTracks = DB.LoadTracks(ref TrackList);
                ReOrderTrackList(Settings.viewStyle);
                TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
                InfoTabEditButton.BackgroundImage = Image.FromFile("Sources\\Images\\pencilGray.png");
                if(MPlayer.TracksTimestamp == InfoTab.Timestamp)
                {
                    MPlayer.SetTitle(newName, ref MusicPlayerTrackLabel);
                    MPlayer.SetAlbumPhoto(TrackList[GetTrackPointer(InfoTab.Timestamp)].T_photo, ref MusicPlayerAlbumPhoto);
                }
            }
        }

        private void FavIcon_Click(object sender, EventArgs e)
        {
            int trackPointer = GetTrackPointer(InfoTab.Timestamp);
            if (!TrackList[trackPointer].T_favorite)
            {
                string query = "UPDATE Tracks SET Favorite='1' WHERE Timestamp='" + TrackList[trackPointer].T_timestamp + "'";
                DB.Update(query);
                FavIcon.BackgroundImage = Image.FromFile("Sources\\Images\\Fav.png"); ;
            }
            else
            {
                string query = "UPDATE Tracks SET Favorite='0' WHERE Timestamp='" + TrackList[trackPointer].T_timestamp + "'";
                DB.Update(query);
                FavIcon.BackgroundImage = Image.FromFile("Sources\\Images\\notFav.png");
            }
            totalTracks = DB.LoadTracks(ref TrackList);
        }

        private void UpdateCoverButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd1 = new OpenFileDialog();
            ofd1.Filter = "Image Files(*.jpg, *.jpeg)|*.JPG;*.JPEG";
            ofd1.Multiselect = false;
            if(ofd1.ShowDialog() == DialogResult.OK)
            {
                string fileExt = Path.GetExtension(ofd1.FileName);
                System.IO.File.Copy(ofd1.FileName, Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Sources\\Database\\TrackCover\\"+ InfoTab.Timestamp + fileExt), true);
                string query = "UPDATE Tracks SET Albums_photo_path='"+ InfoTab.Timestamp + fileExt + "' WHERE Timestamp='"+ InfoTab.Timestamp + "'";
                DB.Update(query);
                Image tmpImage;
                using (var bmpTemp = new Bitmap("Sources\\Database\\TrackCover\\" + InfoTab.Timestamp + fileExt))
                {
                    tmpImage = new Bitmap(bmpTemp);
                    AlbumMainCoverImg.BackgroundImage = tmpImage;
                }
            }
        }

        private void addTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 AddTrackForm = new Form2(ref RefreshTimer);
            AddTrackForm.Show();
        }

        private void addMultipleTracksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 AddMultipleTracksForm = new Form3(ref RefreshTimer);
            AddMultipleTracksForm.Show();
        }


        private void playStyleImage_Click(object sender, EventArgs e)
        {
            if(Settings.playStyle == "Next")
            {
                Settings.playStyle = "Random";
                playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\random.png");
                int randomTrack = randomNum.Next(0, totalTracks);
                MPlayer.SetAudioFile(TrackList[randomTrack].T_audio, TrackList[randomTrack].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[randomTrack].T_photo, TrackList[randomTrack].T_name);
                MPlayer.Play();
            }
            else
            {
                Settings.playStyle = "Next";
                playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\right-arrow.png");
                MPlayer.SetAudioFile(TrackList[0].T_audio, TrackList[0].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[0].T_photo, TrackList[0].T_name);
                MPlayer.Play();
            }
            UpdatePlayingTrackPlays();
        }

        private void dateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Settings.viewStyle != 0) // View style already set
            {
                ReOrderTrackList(0);
                Settings.viewStyle = 0;
                dateToolStripMenuItem.Checked = true;
                playsToolStripMenuItem.Checked = false;

            }
        }

        private void playsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Settings.viewStyle != 1) // View style already set
            {
                ReOrderTrackList(1);
                Settings.viewStyle = 1;
                dateToolStripMenuItem.Checked = false;
                playsToolStripMenuItem.Checked = true;
            }
        }


        private void InfoTabDeleteButton_Click(object sender, EventArgs e)
        {
            string deletedTrackTimestamp = InfoTab.Timestamp;
            if (deletedTrackTimestamp != MPlayer.TracksTimestamp)
            {
                DialogResult finalCheckDialog = MessageBox.Show("Are you sure you want to remove this track from the playlist?", "Remove Track", MessageBoxButtons.YesNo);
                if(finalCheckDialog == DialogResult.Yes)
                {
                    string query = "DELETE * FROM Tracks WHERE Timestamp='"+ deletedTrackTimestamp + "'"; // Remove data from database
                    try
                    {
                        ClearInfoTab(); // Clear data in infotab
                        System.IO.File.Delete(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Sources\\Database\\TrackAudio\\" + TrackList[GetTrackPointer(deletedTrackTimestamp)].T_audio));                    
                    if(TrackList[GetTrackPointer(deletedTrackTimestamp)].T_photo != "none")
                    {
                        System.IO.File.Delete(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Sources\\Database\\TrackCover\\" + TrackList[GetTrackPointer(deletedTrackTimestamp)].T_photo));
                    }
                    DB.Update(query);
                    ClearInfoTab(); // Clear data in infotab    
                    totalTracks--;
                    DB.LoadTracks(ref TrackList);
                    ReOrderTrackList(Settings.viewStyle);
                    TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
                    }
                    catch (IOException) {
                        // Some files are missing but it's ok because we are removing them anyway
                    };
                }
                else
                {
                    return; // abort track removal
                }
            }
            else
            {
                MessageBox.Show("This track is now playing and can not be deleted!");
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            totalTracks = DB.LoadTracks(ref TrackList);
            ReOrderTrackList(Settings.viewStyle);
            TrackViewList.toListView(ref TrackViewList1, TrackList, Settings.viewStyle);
        }

        private void playPauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainPPButton.PerformClick();
        }

        private void nextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainNextTrackButton.PerformClick();
        }

        private void previousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainPrevTrackButton.PerformClick();
        }

        private void randomPlayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.playStyle = "Random";
            playStyleImage.BackgroundImage = Image.FromFile("Sources\\Images\\random.png");
            int randomTrack = randomNum.Next(0, totalTracks);
            MPlayer.SetAudioFile(TrackList[randomTrack].T_audio, TrackList[randomTrack].T_timestamp, ref MusicPlayerTrackLabel, ref MusicPlayerAlbumPhoto, TrackList[randomTrack].T_photo, TrackList[randomTrack].T_name);
            MPlayer.Play();
            UpdatePlayingTrackPlays();
        }

        private void PopMessegeBox(string message)
        {
            MessageBox.Show(message, "Help Window");
        }

        private void howToAddANewTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "Go to 'File -> Add Track' or 'File -> Add Multiple Tracks'";
            PopMessegeBox(msg);
        }

        private void howToEditSongInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "Select from the list the track you want to edit.\n\nIn the infotab panel (Right panel) click the button with the pencil.\n\nThe track info will be enabled for editting.\n\nAlso a button in the top left corner of the album photo will appear.\n\nFrom there you can upload an album photo for the track.\n\nWhen you're done click the button with the green check mark.\n\nYou're Done!";
            PopMessegeBox(msg);
        }

        private void howToEnableRandomPlayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "Option 1:\n\nIn the music player panel (Bottom panel) click the right arrow.\nThe random play will be enabled. Click the same button to disable it.\n\nOption 2:\n\nGo to 'Player -> Random Play' and you're done.";
            PopMessegeBox(msg);
        }

        private void progressBar1_Click(object sender, EventArgs e)
        { 
            if(MPlayer.TracksTimestamp != "")
            {
                double mouseX = (double) progressBar1.PointToClient(Cursor.Position).X;
                MPlayer.ChangePosition(mouseX);
            }
        }

        private void progressBar1_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void progressBar1_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void playStyleImage_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void playStyleImage_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void FavIcon_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void FavIcon_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            RefreshTimer.Enabled = false;
            refreshToolStripMenuItem.PerformClick();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }

    public class TrackItem
    {
        public int T_id;
        public int T_plays;
        public string T_timestamp;
        public string T_name;
        public string T_artist;
        public string T_album;
        public string T_addedDate;
        public string T_photo;
        public string T_audio;
        public bool T_favorite;

        public TrackItem(int T_id, string T_name, string T_artist, string T_album, string T_addedDate, int T_plays, bool T_favorite, string T_photo, string T_timestamp, string T_audio)
        {
            this.T_id = T_id;
            this.T_name = T_name;
            this.T_artist = T_artist;
            this.T_album = T_album;
            this.T_addedDate = T_addedDate;
            this.T_plays = T_plays;
            this.T_favorite = T_favorite;
            this.T_photo = T_photo;
            this.T_timestamp = T_timestamp;
            this.T_audio = T_audio;
        }
    }

    public class ViewListHandler
    {
        public ViewListHandler()
        {

        }

        public void toListView(ref ListView TrackViewList, TrackItem[] TrackList, int mode)
        {
            TrackViewList.Items.Clear();
            foreach (TrackItem Track in TrackList)
            {
                if(mode == 0 || mode == 1)
                {
                    ListViewItem ListViewItem = new ListViewItem(Track.T_name);
                    ListViewItem.SubItems.Add(Track.T_artist);
                    ListViewItem.SubItems.Add(Track.T_album);
                    ListViewItem.SubItems.Add(Track.T_addedDate);
                    ListViewItem.SubItems.Add(Track.T_plays.ToString());
                    ListViewItem.SubItems.Add(Track.T_timestamp);
                    TrackViewList.Items.Add(ListViewItem);
                }
                else if(mode == 2)
                {
                    if (Track.T_favorite)
                    {
                        ListViewItem ListViewItem = new ListViewItem(Track.T_name);
                        ListViewItem.SubItems.Add(Track.T_artist);
                        ListViewItem.SubItems.Add(Track.T_album);
                        ListViewItem.SubItems.Add(Track.T_addedDate);
                        ListViewItem.SubItems.Add(Track.T_plays.ToString());
                        ListViewItem.SubItems.Add(Track.T_timestamp);
                        TrackViewList.Items.Add(ListViewItem);
                    }
                }
            }
        }
    }



    public class OptionsHandler
    {
        public string playStyle = "Next";
        public int viewStyle = 0;

        public void setPlayStyle(string pStyle)
        {
            playStyle = pStyle;
        }

        public void setViewStyle(int vStyle)
        {
            viewStyle = vStyle;
        }


    }

    public class MusicPlayerHandler
    {
        public WindowsMediaPlayer Player;
        Button MainPPButton;
        Button MainPrevTrackButton;
        Button MainNextTrackButton;
        Timer MusicTimer;
        int totalTracks;
        TrackItem[] TrackList;
        string CurrentUrl = "";
        public string TracksTimestamp = "";
        string trackDurationString = "00:00";
        double trackDurationDouble;
        public string Status = "Ready";
        bool durationFlag = false;

        public MusicPlayerHandler(ref Button MainPPButton, ref Button MainPrevTrackButton, ref Button MainNextTrackButton, int totalTracks, TrackItem[] TrackList, ref Timer MusicTimer)
        {
            Player = new WindowsMediaPlayer(); // Initiate music player object
            Player.settings.autoStart = false; // Set autoplay to false
            Player.settings.volume = 50;
            this.MainPPButton = MainPPButton;
            this.MainPrevTrackButton = MainPrevTrackButton;
            this.MainNextTrackButton = MainNextTrackButton;
            this.totalTracks = totalTracks;
            this.MusicTimer = MusicTimer;
            this.TrackList = TrackList;
        }

        public void SetAudioFile(string url, string timestamp, ref Label MainTrackLabel, ref PictureBox MainCoverPhoto, string CoverPath, string TrackTitle)
        {
            CurrentUrl = "Sources\\Database\\TrackAudio\\" + url;
            Player.URL = CurrentUrl;
            TracksTimestamp = timestamp;
            MainTrackLabel.Text = TrackTitle;
            if (CoverPath == "none")
            {
                MainCoverPhoto.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
            }
            else
            {
                try
                {
                    Image tmpImage;
                    using (var bmpTemp = new Bitmap("Sources\\Database\\TrackCover\\" + CoverPath))
                    {
                        tmpImage = new Bitmap(bmpTemp);
                        MainCoverPhoto.BackgroundImage = tmpImage;
                    }
                }
                catch (System.ArgumentException g)
                {
                    MainCoverPhoto.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                }
            }

            MainTrackLabel.Visible = true;
            MainCoverPhoto.Visible = true;
            trackDurationString = "";
            durationFlag = false;
        }

        public void SetTitle(string title, ref Label MainTrackLabel)
        {
            MainTrackLabel.Text = title;
        }

        public void SetAlbumPhoto(string file, ref PictureBox MainCoverPhoto)
        {
            if(file == "none")
            {
                MainCoverPhoto.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
            }
            else
            {
                try
                {
                    Image tmpImage;
                    using (var bmpTemp = new Bitmap("Sources\\Database\\TrackCover\\" + file))
                    {
                        tmpImage = new Bitmap(bmpTemp);
                        MainCoverPhoto.BackgroundImage = tmpImage;
                    }
                }
                catch (FileNotFoundException e)
                {
                    MainCoverPhoto.BackgroundImage = Image.FromFile("Sources\\Images\\album_image.png");
                }
            }
        }

        public void ChangePosition(double mouseXPos)
        {
            double maxXPos = 429.0;
            Player.controls.currentPosition = trackDurationDouble*mouseXPos/maxXPos;
        }

        public string GetState()
        {
            return Player.playState.ToString();
        }

        public void Play()
        {
            Player.controls.play();
            Status = "Playing";
            MusicTimer.Enabled = true;
            MainPPButton.BackgroundImage = Image.FromFile("Sources\\Images\\pause-symbol.png");
        }

        public void Pause()
        {
            Player.controls.pause();
            Status = "Paused";
            MusicTimer.Enabled = false;
            MainPPButton.BackgroundImage = Image.FromFile("Sources\\Images\\play-button.png");
        }

        public string GetDuretionString()
        {
            return trackDurationString;
        }

        public double GetDurationDouble()
        {
            return trackDurationDouble;
        }

        public string GetPositionString()
        {
            return Player.controls.currentPositionString;
        }

        public double GetPositionDouble()
        {
            return Player.controls.currentPosition;
        }

        public void SetVolume(int value)
        {
            Player.settings.volume = value;
        }


        public void setDuration()
        {
            if (durationFlag == false && Player.controls.currentItem.durationString != "00:00")
            {
                trackDurationString = Player.controls.currentItem.durationString;
                trackDurationDouble = Player.controls.currentItem.duration;
                durationFlag = true;
            }
        }
    }


    public class InfoTabHandler
    {
        TextBox NameBox;
        TextBox ArtistBox;
        TextBox AlbumBox;
        TextBox DateBox;
        TextBox PlaysBox;
        PictureBox FavIcon;
        public string Timestamp;
        public string State = "preview";

        public InfoTabHandler(ref TextBox NameBox, ref TextBox ArtistBox, ref TextBox AlbumBox, ref TextBox DateBox, ref TextBox PlaysBox, ref PictureBox FavIcon)
        {
            this.NameBox = NameBox;
            this.ArtistBox = ArtistBox;
            this.AlbumBox = AlbumBox;
            this.DateBox = DateBox;
            this.PlaysBox = PlaysBox;
            this.FavIcon = FavIcon;
        }

        public void updatePlays(int plays)
        {
            PlaysBox.Text = plays.ToString();
        }

        public void setData(string name, string artist, string album, string date, int plays, bool favorite, string Timestamp)
        {

            this.Timestamp = Timestamp;
            NameBox.Text = name;
            ArtistBox.Text = artist;
            AlbumBox.Text = album;
            DateBox.Text = date;
            PlaysBox.Text = plays.ToString();
            if (favorite)
            {
                FavIcon.BackgroundImage = Image.FromFile("Sources\\Images\\Fav.png");
                FavIcon.Visible = true;
            }
            else
            {
                FavIcon.BackgroundImage = Image.FromFile("Sources\\Images\\notFav.png");
                FavIcon.Visible = true;
            }
        }
    }



    public class DatabaseHandler
    {
        public OleDbConnection DBconn;
        string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Sources\\Database\\TruckDB.mdb";

        public DatabaseHandler()
        {

        }

        public void Connect()
        {
            try
            {
                DBconn = new OleDbConnection(ConnectionString);
            }
            catch (System.Data.OleDb.OleDbException g)
            {
                MessageBox.Show("Connection with Database Failed!");
                Application.Exit();
            }
        }

        public int LoadTracks(ref TrackItem[] TrackList)
        {

            DBconn.Open();
            string Query = "SELECT COUNT(*) FROM Tracks";
            OleDbCommand DBCommand = new OleDbCommand(Query, DBconn);
            int totalTracks = Convert.ToInt32(DBCommand.ExecuteScalar());
            Query = "SELECT * FROM Tracks";
            DBCommand = new OleDbCommand(Query, DBconn);
            TrackList = new TrackItem[totalTracks];
            OleDbDataReader DataReader = DBCommand.ExecuteReader();
            int pointer = 0;
            while (DataReader.Read())
            {
                TrackList[pointer++] = new TrackItem(DataReader.GetInt32(0), DataReader.GetString(1), DataReader.GetString(2), DataReader.GetString(3), DataReader.GetString(4), DataReader.GetInt32(5), DataReader.GetBoolean(6), DataReader.GetString(7), DataReader.GetString(8), DataReader.GetString(9));
            }
            DBconn.Close();
            return totalTracks;
        }

        public void Update(string query)
        {
            DBconn.Open();
            OleDbCommand updateCommand = new OleDbCommand(query, DBconn);
            updateCommand.ExecuteNonQuery();
            DBconn.Close();
        }

        public void Update(string query, OleDbCommand updateCommand)
        {
            DBconn.Open();
            updateCommand.ExecuteNonQuery();
            DBconn.Close();
        }
    }
}
