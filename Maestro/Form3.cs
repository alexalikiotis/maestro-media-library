﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maestro
{
    public partial class Form3 : Form
    {
        DatabaseHandler DB;
        string thisDay;
        WMPLib.WindowsMediaPlayer trackAudio = new WMPLib.WindowsMediaPlayer();
        System.Windows.Forms.Timer parentFormTimer;
        int totalTracksToAdd = 0;
        int tracksAdded = 0;
        int totalErrors = 0;

        public Form3(ref System.Windows.Forms.Timer parentFormTimer)
        {
            InitializeComponent();
            this.parentFormTimer = parentFormTimer;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            thisDay = DateTime.Today.ToString("d");
            DB = new DatabaseHandler();
            DB.Connect();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Audio Files(*.wav, *.mp3, *mp4)|*.MP3;*.MP4;*WAV";
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                totalTracksToAdd = ofd.FileNames.Length;
                progressBar1.Visible = true;
                label1.Visible = true;
                button1.Visible = false;
                timer1.Enabled = true;
                foreach(string file in ofd.FileNames)
                {
                    string tracksTimestamp = "";
                    string musicFilename;
                    string musicFileExt;
                    string tracksName;
                    string tracksArtist;
                    string tracksAlbum;
                    DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    long unixTimestamp = (long)(DateTime.Now - sTime).TotalSeconds;
                    tracksTimestamp = unixTimestamp.ToString();
                    musicFilename = file;
                    musicFileExt = Path.GetExtension(file);
                    trackAudio.URL = musicFilename;
                    trackAudio.controls.play();
                    trackAudio.settings.volume = 0;
                    await Task.Delay(500);
                    if (trackAudio.currentMedia.getItemInfo("Title") == "")
                    {
                        tracksName = trackAudio.currentMedia.getItemInfo("Name");
                    }
                    else
                    {
                        tracksName = trackAudio.currentMedia.getItemInfo("Title");
                    }
                    tracksArtist = trackAudio.currentMedia.getItemInfo("Artist");
                    tracksAlbum = trackAudio.currentMedia.getItemInfo("Album");
                    trackAudio.controls.stop();
                    try
                    {
                        System.IO.File.Copy(musicFilename, Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Sources\\Database\\TrackAudio\\" + tracksTimestamp + musicFileExt), true);
                        string query = "INSERT INTO Tracks (Track_name,[Artist],[Track_album],[Added_date],[Plays],[Favorite],[Albums_photo_path],[Timestamp],[Audio_file_path]) VALUES (@Track_name,@Artist,@Track_album,@Added_date,@Plays,@Favorite,@Albums_photo_path,@Timestamp,@Audio_file_path)";
                        OleDbCommand updateCommand = new OleDbCommand(query, DB.DBconn);
                        updateCommand.Parameters.AddWithValue("@Track_name", tracksName);
                        updateCommand.Parameters.AddWithValue("@Artist", tracksArtist);
                        updateCommand.Parameters.AddWithValue("@Track_album", tracksAlbum);
                        updateCommand.Parameters.AddWithValue("@Added_date", thisDay);
                        updateCommand.Parameters.AddWithValue("@Plays", 0);
                        updateCommand.Parameters.AddWithValue("@Favorite", false);
                        updateCommand.Parameters.AddWithValue("@Albums_photo_path", "none");
                        updateCommand.Parameters.AddWithValue("@Timestamp", tracksTimestamp);
                        updateCommand.Parameters.AddWithValue("@Audio_file_path", tracksTimestamp + musicFileExt);
                        DB.Update(query, updateCommand);
                        tracksAdded++;
                    }
                    catch (IOException)
                    {
                        totalErrors++;
                    }
                    await Task.Delay(600); // slow down the proccess
                }
                if(totalErrors > 0)
                {
                    MessageBox.Show("Some files couldn't be loaded!");
                }
                timer1.Enabled = false;
                parentFormTimer.Enabled = true;
                this.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = Convert.ToInt32(100*tracksAdded/totalTracksToAdd);
        }
    }
}
