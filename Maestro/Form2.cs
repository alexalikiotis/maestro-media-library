﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maestro
{
    public partial class Form2 : Form
    {
        DatabaseHandler DB;
        string thisDay;
        string tracksTimestamp;
        string musicFileExt;
        string musicFilename;
        WMPLib.WindowsMediaPlayer trackAudio = new WMPLib.WindowsMediaPlayer();
        System.Windows.Forms.Timer parentFormTimer;

        public Form2(ref System.Windows.Forms.Timer parentFormTimer)
        {
            InitializeComponent();
            this.parentFormTimer = parentFormTimer;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            thisDay = DateTime.Today.ToString("d");
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long unixTimestamp = (long) (DateTime.Now - sTime).TotalSeconds;
            tracksTimestamp = unixTimestamp.ToString();
            DB = new DatabaseHandler();
            DB.Connect();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.Copy(musicFilename, Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Sources\\Database\\TrackAudio\\" + tracksTimestamp + musicFileExt), true);
                string query = "INSERT INTO Tracks (Track_name,[Artist],[Track_album],[Added_date],[Plays],[Favorite],[Albums_photo_path],[Timestamp],[Audio_file_path]) VALUES (@Track_name,@Artist,@Track_album,@Added_date,@Plays,@Favorite,@Albums_photo_path,@Timestamp,@Audio_file_path)";
                OleDbCommand updateCommand = new OleDbCommand(query, DB.DBconn);
                updateCommand.Parameters.AddWithValue("@Track_name", textBox1.Text);
                updateCommand.Parameters.AddWithValue("@Artist", textBox2.Text);
                updateCommand.Parameters.AddWithValue("@Track_album", textBox3.Text);
                updateCommand.Parameters.AddWithValue("@Added_date", thisDay);
                updateCommand.Parameters.AddWithValue("@Plays", 0);
                updateCommand.Parameters.AddWithValue("@Favorite", false);
                updateCommand.Parameters.AddWithValue("@Albums_photo_path", "none");
                updateCommand.Parameters.AddWithValue("@Timestamp", tracksTimestamp);
                updateCommand.Parameters.AddWithValue("@Audio_file_path", tracksTimestamp + musicFileExt);
                DB.Update(query, updateCommand);
                parentFormTimer.Enabled = true;
                this.Close();
            }
            catch (IOException)
            {
                MessageBox.Show("An Error was occurred while copying the audio file!");
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Audio Files(*.wav, *.mp3, *mp4)|*.MP3;*.MP4;*WAV";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                musicFilename = ofd.FileName;
                musicFileExt = Path.GetExtension(ofd.FileName);
                trackAudio.URL = musicFilename;
                trackAudio.controls.play();
                trackAudio.settings.volume = 0;
                timer1.Enabled = true;
                await Task.Delay(500);
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                if (trackAudio.currentMedia.getItemInfo("Title") == "")
                {
                    textBox1.Text = trackAudio.currentMedia.getItemInfo("Name");
                }
                else
                {
                    textBox1.Text = trackAudio.currentMedia.getItemInfo("Title");
                }
                button1.Enabled = true;
                textBox2.Text = trackAudio.currentMedia.getItemInfo("Artist");
                textBox3.Text = trackAudio.currentMedia.getItemInfo("Album");
                trackAudio.controls.stop();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }
    }
}
