﻿namespace Maestro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMultipleTracksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomPlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToAddANewTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToEditSongInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToEnableRandomPlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.playStyleImage = new System.Windows.Forms.PictureBox();
            this.TrackDurationLabel = new System.Windows.Forms.Label();
            this.TrackPositionLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.volumeBar = new System.Windows.Forms.TrackBar();
            this.MusicPlayerTrackLabel = new System.Windows.Forms.Label();
            this.MusicPlayerAlbumPhoto = new System.Windows.Forms.PictureBox();
            this.MainNextTrackButton = new System.Windows.Forms.Button();
            this.MainPPButton = new System.Windows.Forms.Button();
            this.MainPrevTrackButton = new System.Windows.Forms.Button();
            this.TrackViewList1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AlbumMainCoverImg = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.InfoTabDeleteButton = new System.Windows.Forms.Button();
            this.InfoTabEditButton = new System.Windows.Forms.Button();
            this.InfoTabPlayButton = new System.Windows.Forms.Button();
            this.FavIcon = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.PlaysBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DateBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AlbumBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ArtistBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.UpdateCoverButton = new System.Windows.Forms.Button();
            this.MusicTimer = new System.Windows.Forms.Timer(this.components);
            this.RefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playStyleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MusicPlayerAlbumPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlbumMainCoverImg)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FavIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.playToolStripMenuItem,
            this.playerToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1278, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTrackToolStripMenuItem,
            this.addMultipleTracksToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addTrackToolStripMenuItem
            // 
            this.addTrackToolStripMenuItem.Name = "addTrackToolStripMenuItem";
            this.addTrackToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addTrackToolStripMenuItem.Text = "Add Track";
            this.addTrackToolStripMenuItem.Click += new System.EventHandler(this.addTrackToolStripMenuItem_Click);
            // 
            // addMultipleTracksToolStripMenuItem
            // 
            this.addMultipleTracksToolStripMenuItem.Name = "addMultipleTracksToolStripMenuItem";
            this.addMultipleTracksToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addMultipleTracksToolStripMenuItem.Text = "Add Multiple Tracks";
            this.addMultipleTracksToolStripMenuItem.Click += new System.EventHandler(this.addMultipleTracksToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // playToolStripMenuItem
            // 
            this.playToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderByToolStripMenuItem,
            this.refreshToolStripMenuItem});
            this.playToolStripMenuItem.Name = "playToolStripMenuItem";
            this.playToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.playToolStripMenuItem.Text = "View";
            // 
            // orderByToolStripMenuItem
            // 
            this.orderByToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateToolStripMenuItem,
            this.playsToolStripMenuItem});
            this.orderByToolStripMenuItem.Name = "orderByToolStripMenuItem";
            this.orderByToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.orderByToolStripMenuItem.Text = "Order By";
            // 
            // dateToolStripMenuItem
            // 
            this.dateToolStripMenuItem.Checked = true;
            this.dateToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dateToolStripMenuItem.Name = "dateToolStripMenuItem";
            this.dateToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.dateToolStripMenuItem.Text = "Date";
            this.dateToolStripMenuItem.Click += new System.EventHandler(this.dateToolStripMenuItem_Click);
            // 
            // playsToolStripMenuItem
            // 
            this.playsToolStripMenuItem.Name = "playsToolStripMenuItem";
            this.playsToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.playsToolStripMenuItem.Text = "Plays";
            this.playsToolStripMenuItem.Click += new System.EventHandler(this.playsToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // playerToolStripMenuItem
            // 
            this.playerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playPauseToolStripMenuItem,
            this.nextToolStripMenuItem,
            this.previousToolStripMenuItem,
            this.randomPlayToolStripMenuItem});
            this.playerToolStripMenuItem.Name = "playerToolStripMenuItem";
            this.playerToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.playerToolStripMenuItem.Text = "Player";
            // 
            // playPauseToolStripMenuItem
            // 
            this.playPauseToolStripMenuItem.Name = "playPauseToolStripMenuItem";
            this.playPauseToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.playPauseToolStripMenuItem.Text = "Play/Pause";
            this.playPauseToolStripMenuItem.Click += new System.EventHandler(this.playPauseToolStripMenuItem_Click);
            // 
            // nextToolStripMenuItem
            // 
            this.nextToolStripMenuItem.Name = "nextToolStripMenuItem";
            this.nextToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.nextToolStripMenuItem.Text = "Next";
            this.nextToolStripMenuItem.Click += new System.EventHandler(this.nextToolStripMenuItem_Click);
            // 
            // previousToolStripMenuItem
            // 
            this.previousToolStripMenuItem.Name = "previousToolStripMenuItem";
            this.previousToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.previousToolStripMenuItem.Text = "Previous";
            this.previousToolStripMenuItem.Click += new System.EventHandler(this.previousToolStripMenuItem_Click);
            // 
            // randomPlayToolStripMenuItem
            // 
            this.randomPlayToolStripMenuItem.Name = "randomPlayToolStripMenuItem";
            this.randomPlayToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.randomPlayToolStripMenuItem.Text = "Random Play";
            this.randomPlayToolStripMenuItem.Click += new System.EventHandler(this.randomPlayToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToAddANewTrackToolStripMenuItem,
            this.howToEditSongInfoToolStripMenuItem,
            this.howToEnableRandomPlayToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // howToAddANewTrackToolStripMenuItem
            // 
            this.howToAddANewTrackToolStripMenuItem.Name = "howToAddANewTrackToolStripMenuItem";
            this.howToAddANewTrackToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.howToAddANewTrackToolStripMenuItem.Text = "Add new tracks";
            this.howToAddANewTrackToolStripMenuItem.Click += new System.EventHandler(this.howToAddANewTrackToolStripMenuItem_Click);
            // 
            // howToEditSongInfoToolStripMenuItem
            // 
            this.howToEditSongInfoToolStripMenuItem.Name = "howToEditSongInfoToolStripMenuItem";
            this.howToEditSongInfoToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.howToEditSongInfoToolStripMenuItem.Text = "Edit song info";
            this.howToEditSongInfoToolStripMenuItem.Click += new System.EventHandler(this.howToEditSongInfoToolStripMenuItem_Click);
            // 
            // howToEnableRandomPlayToolStripMenuItem
            // 
            this.howToEnableRandomPlayToolStripMenuItem.Name = "howToEnableRandomPlayToolStripMenuItem";
            this.howToEnableRandomPlayToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.howToEnableRandomPlayToolStripMenuItem.Text = "Enable random play";
            this.howToEnableRandomPlayToolStripMenuItem.Click += new System.EventHandler(this.howToEnableRandomPlayToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.panel1.Controls.Add(this.playStyleImage);
            this.panel1.Controls.Add(this.TrackDurationLabel);
            this.panel1.Controls.Add(this.TrackPositionLabel);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.volumeBar);
            this.panel1.Controls.Add(this.MusicPlayerTrackLabel);
            this.panel1.Controls.Add(this.MusicPlayerAlbumPhoto);
            this.panel1.Controls.Add(this.MainNextTrackButton);
            this.panel1.Controls.Add(this.MainPPButton);
            this.panel1.Controls.Add(this.MainPrevTrackButton);
            this.panel1.Location = new System.Drawing.Point(0, 649);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1278, 63);
            this.panel1.TabIndex = 1;
            // 
            // playStyleImage
            // 
            this.playStyleImage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("playStyleImage.BackgroundImage")));
            this.playStyleImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.playStyleImage.Location = new System.Drawing.Point(846, 19);
            this.playStyleImage.Name = "playStyleImage";
            this.playStyleImage.Size = new System.Drawing.Size(28, 29);
            this.playStyleImage.TabIndex = 12;
            this.playStyleImage.TabStop = false;
            this.playStyleImage.Click += new System.EventHandler(this.playStyleImage_Click);
            this.playStyleImage.MouseEnter += new System.EventHandler(this.playStyleImage_MouseEnter);
            this.playStyleImage.MouseLeave += new System.EventHandler(this.playStyleImage_MouseLeave);
            // 
            // TrackDurationLabel
            // 
            this.TrackDurationLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.TrackDurationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.TrackDurationLabel.Location = new System.Drawing.Point(720, 42);
            this.TrackDurationLabel.Name = "TrackDurationLabel";
            this.TrackDurationLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TrackDurationLabel.Size = new System.Drawing.Size(97, 13);
            this.TrackDurationLabel.TabIndex = 11;
            this.TrackDurationLabel.Text = "00:00";
            this.TrackDurationLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // TrackPositionLabel
            // 
            this.TrackPositionLabel.AutoSize = true;
            this.TrackPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.TrackPositionLabel.Location = new System.Drawing.Point(382, 42);
            this.TrackPositionLabel.Name = "TrackPositionLabel";
            this.TrackPositionLabel.Size = new System.Drawing.Size(34, 13);
            this.TrackPositionLabel.TabIndex = 10;
            this.TrackPositionLabel.Text = "00:00";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(227, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 24);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(384, 28);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(430, 10);
            this.progressBar1.TabIndex = 7;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            this.progressBar1.MouseEnter += new System.EventHandler(this.progressBar1_MouseEnter);
            this.progressBar1.MouseLeave += new System.EventHandler(this.progressBar1_MouseLeave);
            // 
            // volumeBar
            // 
            this.volumeBar.Location = new System.Drawing.Point(250, 23);
            this.volumeBar.Maximum = 100;
            this.volumeBar.Name = "volumeBar";
            this.volumeBar.Size = new System.Drawing.Size(104, 45);
            this.volumeBar.TabIndex = 6;
            this.volumeBar.Value = 50;
            this.volumeBar.ValueChanged += new System.EventHandler(this.volumeBar_ValueChanged);
            // 
            // MusicPlayerTrackLabel
            // 
            this.MusicPlayerTrackLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.MusicPlayerTrackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.MusicPlayerTrackLabel.Location = new System.Drawing.Point(912, 21);
            this.MusicPlayerTrackLabel.Name = "MusicPlayerTrackLabel";
            this.MusicPlayerTrackLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MusicPlayerTrackLabel.Size = new System.Drawing.Size(272, 23);
            this.MusicPlayerTrackLabel.TabIndex = 5;
            this.MusicPlayerTrackLabel.Text = "Tracks Name";
            this.MusicPlayerTrackLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MusicPlayerTrackLabel.Visible = false;
            // 
            // MusicPlayerAlbumPhoto
            // 
            this.MusicPlayerAlbumPhoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MusicPlayerAlbumPhoto.BackgroundImage")));
            this.MusicPlayerAlbumPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MusicPlayerAlbumPhoto.Location = new System.Drawing.Point(1190, 2);
            this.MusicPlayerAlbumPhoto.Name = "MusicPlayerAlbumPhoto";
            this.MusicPlayerAlbumPhoto.Size = new System.Drawing.Size(63, 59);
            this.MusicPlayerAlbumPhoto.TabIndex = 2;
            this.MusicPlayerAlbumPhoto.TabStop = false;
            this.MusicPlayerAlbumPhoto.Visible = false;
            // 
            // MainNextTrackButton
            // 
            this.MainNextTrackButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MainNextTrackButton.BackgroundImage")));
            this.MainNextTrackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MainNextTrackButton.Location = new System.Drawing.Point(151, 5);
            this.MainNextTrackButton.Name = "MainNextTrackButton";
            this.MainNextTrackButton.Size = new System.Drawing.Size(52, 53);
            this.MainNextTrackButton.TabIndex = 4;
            this.MainNextTrackButton.UseVisualStyleBackColor = true;
            this.MainNextTrackButton.Click += new System.EventHandler(this.MainNextTrackButton_Click);
            // 
            // MainPPButton
            // 
            this.MainPPButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MainPPButton.BackgroundImage")));
            this.MainPPButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MainPPButton.Location = new System.Drawing.Point(86, 5);
            this.MainPPButton.Name = "MainPPButton";
            this.MainPPButton.Size = new System.Drawing.Size(52, 53);
            this.MainPPButton.TabIndex = 3;
            this.MainPPButton.UseVisualStyleBackColor = true;
            this.MainPPButton.Click += new System.EventHandler(this.MainPPButton_Click);
            // 
            // MainPrevTrackButton
            // 
            this.MainPrevTrackButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MainPrevTrackButton.BackgroundImage")));
            this.MainPrevTrackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MainPrevTrackButton.Location = new System.Drawing.Point(21, 5);
            this.MainPrevTrackButton.Name = "MainPrevTrackButton";
            this.MainPrevTrackButton.Size = new System.Drawing.Size(52, 53);
            this.MainPrevTrackButton.TabIndex = 2;
            this.MainPrevTrackButton.UseVisualStyleBackColor = true;
            this.MainPrevTrackButton.Click += new System.EventHandler(this.MainPrevTrackButton_Click);
            // 
            // TrackViewList1
            // 
            this.TrackViewList1.AllowColumnReorder = true;
            this.TrackViewList1.AutoArrange = false;
            this.TrackViewList1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.TrackViewList1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.TrackViewList1.Font = new System.Drawing.Font("Lucida Sans Unicode", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.TrackViewList1.FullRowSelect = true;
            this.TrackViewList1.GridLines = true;
            this.TrackViewList1.Location = new System.Drawing.Point(0, 28);
            this.TrackViewList1.MultiSelect = false;
            this.TrackViewList1.Name = "TrackViewList1";
            this.TrackViewList1.Size = new System.Drawing.Size(961, 622);
            this.TrackViewList1.TabIndex = 2;
            this.TrackViewList1.UseCompatibleStateImageBehavior = false;
            this.TrackViewList1.View = System.Windows.Forms.View.Details;
            this.TrackViewList1.Click += new System.EventHandler(this.TrackViewList1_Click);
            this.TrackViewList1.DoubleClick += new System.EventHandler(this.TrackViewList1_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Title";
            this.columnHeader1.Width = 326;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Artist";
            this.columnHeader2.Width = 223;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Album";
            this.columnHeader3.Width = 181;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Added";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 141;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Plays";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 86;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Identifier";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader6.Width = 117;
            // 
            // AlbumMainCoverImg
            // 
            this.AlbumMainCoverImg.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.AlbumMainCoverImg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("AlbumMainCoverImg.BackgroundImage")));
            this.AlbumMainCoverImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AlbumMainCoverImg.Location = new System.Drawing.Point(967, 28);
            this.AlbumMainCoverImg.Name = "AlbumMainCoverImg";
            this.AlbumMainCoverImg.Size = new System.Drawing.Size(311, 270);
            this.AlbumMainCoverImg.TabIndex = 3;
            this.AlbumMainCoverImg.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.InfoTabDeleteButton);
            this.groupBox1.Controls.Add(this.InfoTabEditButton);
            this.groupBox1.Controls.Add(this.InfoTabPlayButton);
            this.groupBox1.Controls.Add(this.FavIcon);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.PlaysBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.DateBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.AlbumBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ArtistBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.NameBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(967, 304);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(311, 341);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Track Info";
            // 
            // InfoTabDeleteButton
            // 
            this.InfoTabDeleteButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InfoTabDeleteButton.BackgroundImage")));
            this.InfoTabDeleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.InfoTabDeleteButton.Location = new System.Drawing.Point(272, 295);
            this.InfoTabDeleteButton.Name = "InfoTabDeleteButton";
            this.InfoTabDeleteButton.Size = new System.Drawing.Size(38, 36);
            this.InfoTabDeleteButton.TabIndex = 15;
            this.InfoTabDeleteButton.UseVisualStyleBackColor = true;
            this.InfoTabDeleteButton.Visible = false;
            this.InfoTabDeleteButton.Click += new System.EventHandler(this.InfoTabDeleteButton_Click);
            // 
            // InfoTabEditButton
            // 
            this.InfoTabEditButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InfoTabEditButton.BackgroundImage")));
            this.InfoTabEditButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.InfoTabEditButton.Location = new System.Drawing.Point(228, 295);
            this.InfoTabEditButton.Name = "InfoTabEditButton";
            this.InfoTabEditButton.Size = new System.Drawing.Size(38, 36);
            this.InfoTabEditButton.TabIndex = 14;
            this.InfoTabEditButton.UseVisualStyleBackColor = true;
            this.InfoTabEditButton.Visible = false;
            this.InfoTabEditButton.Click += new System.EventHandler(this.InfoTabEditButton_Click);
            // 
            // InfoTabPlayButton
            // 
            this.InfoTabPlayButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InfoTabPlayButton.BackgroundImage")));
            this.InfoTabPlayButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.InfoTabPlayButton.Location = new System.Drawing.Point(183, 295);
            this.InfoTabPlayButton.Name = "InfoTabPlayButton";
            this.InfoTabPlayButton.Size = new System.Drawing.Size(38, 36);
            this.InfoTabPlayButton.TabIndex = 13;
            this.InfoTabPlayButton.UseVisualStyleBackColor = true;
            this.InfoTabPlayButton.Visible = false;
            this.InfoTabPlayButton.Click += new System.EventHandler(this.InfoTabPlayButton_Click);
            // 
            // FavIcon
            // 
            this.FavIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FavIcon.Location = new System.Drawing.Point(73, 302);
            this.FavIcon.Name = "FavIcon";
            this.FavIcon.Size = new System.Drawing.Size(26, 23);
            this.FavIcon.TabIndex = 12;
            this.FavIcon.TabStop = false;
            this.FavIcon.Visible = false;
            this.FavIcon.Click += new System.EventHandler(this.FavIcon_Click);
            this.FavIcon.MouseEnter += new System.EventHandler(this.FavIcon_MouseEnter);
            this.FavIcon.MouseLeave += new System.EventHandler(this.FavIcon_MouseLeave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label7.Location = new System.Drawing.Point(10, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Favorite:";
            // 
            // PlaysBox
            // 
            this.PlaysBox.Enabled = false;
            this.PlaysBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaysBox.Location = new System.Drawing.Point(9, 258);
            this.PlaysBox.Name = "PlaysBox";
            this.PlaysBox.Size = new System.Drawing.Size(127, 22);
            this.PlaysBox.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label6.Location = new System.Drawing.Point(6, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Plays:";
            // 
            // DateBox
            // 
            this.DateBox.Enabled = false;
            this.DateBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateBox.Location = new System.Drawing.Point(9, 204);
            this.DateBox.Name = "DateBox";
            this.DateBox.Size = new System.Drawing.Size(205, 22);
            this.DateBox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(6, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Added Date:";
            // 
            // AlbumBox
            // 
            this.AlbumBox.Enabled = false;
            this.AlbumBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AlbumBox.Location = new System.Drawing.Point(9, 152);
            this.AlbumBox.Name = "AlbumBox";
            this.AlbumBox.Size = new System.Drawing.Size(302, 22);
            this.AlbumBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(6, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Album:";
            // 
            // ArtistBox
            // 
            this.ArtistBox.Enabled = false;
            this.ArtistBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ArtistBox.Location = new System.Drawing.Point(9, 97);
            this.ArtistBox.Name = "ArtistBox";
            this.ArtistBox.Size = new System.Drawing.Size(302, 22);
            this.ArtistBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(6, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Artist:";
            // 
            // NameBox
            // 
            this.NameBox.Enabled = false;
            this.NameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameBox.Location = new System.Drawing.Point(9, 44);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(302, 22);
            this.NameBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Title:";
            // 
            // UpdateCoverButton
            // 
            this.UpdateCoverButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("UpdateCoverButton.BackgroundImage")));
            this.UpdateCoverButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UpdateCoverButton.Location = new System.Drawing.Point(967, 28);
            this.UpdateCoverButton.Name = "UpdateCoverButton";
            this.UpdateCoverButton.Size = new System.Drawing.Size(38, 36);
            this.UpdateCoverButton.TabIndex = 16;
            this.UpdateCoverButton.UseVisualStyleBackColor = true;
            this.UpdateCoverButton.Visible = false;
            this.UpdateCoverButton.Click += new System.EventHandler(this.UpdateCoverButton_Click);
            // 
            // MusicTimer
            // 
            this.MusicTimer.Interval = 1000;
            this.MusicTimer.Tick += new System.EventHandler(this.MusicTimer_Tick);
            // 
            // RefreshTimer
            // 
            this.RefreshTimer.Interval = 250;
            this.RefreshTimer.Tick += new System.EventHandler(this.RefreshTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(1278, 711);
            this.Controls.Add(this.UpdateCoverButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AlbumMainCoverImg);
            this.Controls.Add(this.TrackViewList1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maestro";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playStyleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MusicPlayerAlbumPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlbumMainCoverImg)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FavIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button MainPrevTrackButton;
        private System.Windows.Forms.Button MainPPButton;
        private System.Windows.Forms.Button MainNextTrackButton;
        private System.Windows.Forms.PictureBox MusicPlayerAlbumPhoto;
        private System.Windows.Forms.ListView TrackViewList1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.PictureBox AlbumMainCoverImg;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label MusicPlayerTrackLabel;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PlaysBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DateBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AlbumBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ArtistBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.PictureBox FavIcon;
        private System.Windows.Forms.Button InfoTabPlayButton;
        private System.Windows.Forms.Button InfoTabDeleteButton;
        private System.Windows.Forms.Button InfoTabEditButton;
        private System.Windows.Forms.TrackBar volumeBar;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer MusicTimer;
        private System.Windows.Forms.Label TrackDurationLabel;
        private System.Windows.Forms.Label TrackPositionLabel;
        private System.Windows.Forms.Button UpdateCoverButton;
        private System.Windows.Forms.ToolStripMenuItem addTrackToolStripMenuItem;
        private System.Windows.Forms.PictureBox playStyleImage;
        private System.Windows.Forms.ToolStripMenuItem orderByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playPauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomPlayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToAddANewTrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToEditSongInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToEnableRandomPlayToolStripMenuItem;
        private System.Windows.Forms.Timer RefreshTimer;
        private System.Windows.Forms.ToolStripMenuItem addMultipleTracksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

