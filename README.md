# README #

Maestro is an open source simple media library developed in C#.

# Developer / Repo owner #

Alex Alikiotis

# Icons Used In Maestro #

Creator: Vaadin - http://www.flaticon.com/authors/vaadin
License: CC 3.0 BY - https://creativecommons.org/licenses/by/3.0/